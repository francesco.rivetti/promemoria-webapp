import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Cat} from "./models/Cat";

function App() {

	/**
	 * I due punti con la tipologia vanno inseriti SOLO IN FASE DI DICHIARAZIONE DELLA VARIABILE
	 */
	var numero: number = 123;
	var stringa: string = "ciao";
	var booleano: boolean = true;
	var arrayStringhe: string[] = ["sssd", "asasas"]; // array di stringhe
	var qualsiasiTipo: any = "sdsdsd";
	var qualsiasiTipoObj: object = {};

	/**
	 * Questa funzione non ha un return quindi il tipo di ritorno si definisce come void
	 *
	 * @param param: è una stringa obbligatoria
	 * @param param2: è un numero obbligatorio
	 * @param param3: è un booleano opzionale in quanto c'è il ? dopo il nome del parametro
	 */
	var myVoidFunc = function (param: string, param2: number, param3?: boolean): void {
		console.log(param, param2, param3);
	}
	myVoidFunc("ciao", 43, false);

	/**
	 * Questa funzione ritorna una stringa!
	 *
	 * @param param: è una stringa obbligatoria
	 * @param param2: è un numero obbligatorio
	 * @param param3: è un booleano opzionale in quanto c'è il ? dopo il nome del parametro
	 */
	var myStringFunc = function (param: string, param2: number, param3?: boolean): string {
		console.log(param, param2, param3);
		return param;
	}
	stringa = myStringFunc("ciao", 43, false);

	// ISTANZIARE CLASSI
	var cat1 = new Cat("#000000", []);
	var cat2 = new Cat("#454545", []);
	console.log(cat1, cat2);

	return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
