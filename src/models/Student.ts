import {Person} from "./Person";
import {Level} from "./Level.enum";

export class Student extends Person {
  level: Level;
  
  constructor(studentName: string, studentSurname: string, level: Level) {
    super(studentName, studentSurname);
    this.level = level;
    console.log(this.age);
  }
  
  study(lesson: string): boolean {
    console.log("My name is " + this.getFullName() + " and I'm studying " + lesson);
    return true;
  }
}
