export class Eye {
	color: string;
	width: number = 100;
	height: number = 50;

	constructor(color: string) {
		this.color = color;
	}

}
